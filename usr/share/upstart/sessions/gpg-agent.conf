description "GPG Agent"
author "Stéphane Graber <stgraber@ubuntu.com>"

start on starting xsession-init and (no-pinentry-gnome3 or started dbus)

pre-start script
    GNUPGHOME=$HOME/.gnupg
    [ -d $GNUPGHOME ] || { stop; exit 0; }

    # /etc/X11/Xsession.d/90gpg-agent starts the agent without DBUS,
    # thus can't use pinentry-gnome3
    # but we know we have dbus now, restart gpg-agent
    gpgconf --kill gpg-agent
    gpgconf --launch gpg-agent
    initctl set-env --global GPG_AGENT_INFO=$GNUPGHOME/S.gpg-agent:0:1

    if [ -n "$(gpgconf --list-options gpg-agent | awk -F: '/^enable-ssh-support:/{ print $10 }')" ]; then
        initctl set-env --global SSH_AUTH_SOCK=$GNUPGHOME/S.gpg-agent.ssh
    fi
end script

post-stop script
    gpgconf --kill gpg-agent
    initctl unset-env --global GPG_AGENT_INFO

    if [ -n "$(gpgconf --list-options gpg-agent | awk -F: '/^enable-ssh-support:/{ print $10 }')" ]; then
        initctl unset-env --global SSH_AUTH_SOCK
    fi
end script
